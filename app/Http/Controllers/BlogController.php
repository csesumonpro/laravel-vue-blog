<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Post;
use App\Category;

class BlogController extends Controller
{

    public function all_post()
    {
        $posts = Post::with('user', 'category')->orderByDesc('id')->get();
        return response()->json([
            'posts' => $posts
        ], 200);
    }
    public function all_category()
    {
        $categories = Category::all();
        return response()->json([
            'categories' => $categories
        ], 200);
    }
    public function post_by_id($id)
    {
        $post = Post::with('user', 'category')->whereId($id)->first();
        return response()->json(['post' => $post], 200);
    }
    public function category_by_post($id)
    {
        $posts = Post::with('user', 'category')->where('cat_id', $id)->get();
        return response()->json(['posts' => $posts], 200);
    }
    public function search()
    {
        $search = \Request::get('search');
        if ($search != null) {
            $posts = Post::where('title', 'LIKE', "%$search")
                ->orWhere('description', 'LIKE', "%$search%")
                ->get();
            return response()->json(['posts' => $posts], 200);
        } else {
            return $this->all_post();
        }




    }

}
