<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $categories = Category::orderBy('id', 'desc')->get();
        return response()->json([
            "categories" => $categories
        ], 200);

    }
    public function add(Request $request)
    {
        $this->validate($request, [
            'cat_name' => 'required|min:2|max:100'
        ]);

        $category = new Category();
        $category->cat_name = $request->cat_name;
        $category->save();
        return response()->json([
            "category" => $category
        ], 200);
    }
    public function delete($id)
    {
        $categories = Category::find($id);
        $categories->delete();
        return response()->json([
            "categories" => $categories
        ], 200);
    }
    public function category($id)
    {
        $category = Category::find($id);
        return response()->json([
            "category" => $category
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'cat_name' => 'required|min:2|max:100'
        ]);

        $category = new Category();
        $category->cat_name = $request->cat_name;
        $category->save();
        return response()->json([
            "category" => $category
        ], 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->cat_name = $request->cat_name;
        $category->save();
        return $category;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        return $category;
    }
}
