<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use App\Category;
use Illuminate\Http\Request;
use Image;
use DB;
use Auth;


class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with('user', 'category')
            ->orderByDesc('id')
            ->get();
        return response()->json([
            'posts' => $posts,
        ], 200);
    }

    public function add(Request $request)
    {

        $this->validate($request, [
            'title' => 'required|min:2|max:100',
            'description' => 'required|min:2|max:1000',
            'cat_id' => 'required',
            // 'image' => 'required',
        ]);
        $cl_pos = strpos($request->photo, ";");
        $sub = substr($request->photo, 0, $cl_pos);
        $ext = explode("/", $sub)[1];
        $name = time() . "." . $ext;
        $image = Image::make($request->photo)->resize(754, 365);
        $destinationPath = public_path() . '/uploadimage/';
        $image->save($destinationPath . $name);

        $post = new Post();
        $post->title = $request->title;
        $post->description = $request->description;
        $post->cat_id = $request->cat_id;
        $post->user_id = Auth::user()->id;
        $post->photo = $name;
        $post->save();
    }
    public function delete($id)
    {
        $post = Post::findOrFail($id);
        $originalPath = public_path() . "/uploadimage/";
        $post_photo = $originalPath . $post->photo;
        if (file_exists($post_photo)) {
            @unlink($post_photo);
        }
        $post->delete();
    }

    public function post($id)
    {
        $post = Post::findOrFail($id);
        return response()->json([
            'post' => $post
        ], 200);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        $this->validate($request, [
            'title' => 'required|min:2|max:100',
            'description' => 'required|min:2|max:1000',
            'cat_id' => 'required',
            // 'image' => 'required',
        ]);

        if ($request->photo != $post->photo) {
            $cl_pos = strpos($request->photo, ";");
            $sub = substr($request->photo, 0, $cl_pos);
            $ext = explode("/", $sub)[1];
            $name = time() . "." . $ext;
            $image = Image::make($request->photo)->resize(754, 365);
            $destinationPath = public_path() . '/uploadimage/';
            $image->save($destinationPath . $name);
            $post_photo = $destinationPath . $post->photo;
            if (file_exists($post_photo)) {
                @unlink($post_photo);
            }
        } else {
            $name = $post->photo;
        }

        $post->title = $request->title;
        $post->description = $request->description;
        $post->cat_id = $request->cat_id;
        $post->user_id = Auth::user()->id;
        $post->photo = $name;
        $post->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
