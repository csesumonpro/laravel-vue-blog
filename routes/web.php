<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('public.master');
});

//Admin Panel Routes
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
// Route::get('{path}', 'HomeController@index')->where('path', '.*');

// Route::resources([
//     'category' => 'CategoryController'
// ]);


 
Route::group(['prefix' => 'category'], function () {
    Route::get('/', 'CategoryController@index');
    Route::post('new', 'CategoryController@add');
    Route::get('delete/{id}', 'CategoryController@delete');
    Route::get('category/{id}', 'CategoryController@category');
    Route::post('update/{id}', 'CategoryController@update');
});
Route::group(['prefix' => 'post'], function () {
    Route::get('/', 'PostController@index');
    Route::post('new', 'PostController@add');
    Route::get('delete/{id}', 'PostController@delete');
    Route::get('post/{id}', 'PostController@post');
    Route::post('update/{id}', 'PostController@update');
});

Route::get('blog', 'BlogController@all_post');
Route::get('blogcategory', 'BlogController@all_category');
Route::get('categorypost/{id}', 'BlogController@category_by_post');
Route::get('post/{id}', 'BlogController@post_by_id');
Route::get('search', 'BlogController@search');

