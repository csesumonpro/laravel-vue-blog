import PublicHome from './components/public/PublicHome.vue';
// Blog
import Blog from './components/public/blog/BlogHome.vue';


export const froutes = [
    {
        path: '/',
        component: PublicHome
    },
    {
        path: '/blog',
        component: Blog
    }
]
