export default {
    state: {
        categories: [],
        posts: [],
        blogpost: [],
        blogcategories: [],
        singlepost: [],
    },
    getters: {
        categories(state) {
            return state.categories
        },
        posts(state) {
            return state.posts
        },
        blogposts(state) {
            return state.blogpost
        },
        blogcategories(state) {
            return state.blogcategories
        },
        singlepost(state) {
            return state.singlepost
        },


    },
    mutations: {
        categories(state, payload) {
            state.categories = payload
        },
        posts(state, payload) {
            state.posts = payload
        },
        allBlogPost(state, payload) {
            state.blogpost = payload
        },
        blogCategories(state, payload) {
            state.blogcategories = payload
        },
        singlePost(state, payload) {
            state.singlepost = payload
        },
        categoryBlogPost(state, payload) {
            state.blogpost = payload
        },
        blogSearchPost(state, payload) {
            state.blogpost = payload
        }

    },
    actions: {
        categories(context) {
            axios.get('category')
                .then((response) => {
                    context.commit('categories', response.data.categories)
                })
        },
        posts(context) {
            axios.get('post')
                .then((response) => {
                    context.commit('posts', response.data.posts)
                })
        },
        allBlogPost(context) {
            axios.get('/blog')
                .then((response) => {
                    context.commit('allBlogPost', response.data.posts)
                })
        },
        blogCategories(context) {
            axios.get('/blogcategory')
                .then((response) => {
                    context.commit('blogCategories', response.data.categories)
                })
        },
        singlePost(context, payload) {
            axios.get('/post/' + payload)
                .then((response) => {
                    context.commit('singlePost', response.data.post)
                })
        },
        categoryBlogPost(context, payload) {
            axios.get('/categorypost/' + payload)
                .then((response) => {
                    console.log(response.data.posts)
                    context.commit('categoryBlogPost', response.data.posts)
                })
        },
        blogSearchPost(context, payload) {
            axios.get('/search?search=' + payload)
                .then((response) => {
                    context.commit('blogSearchPost', response.data.posts)
                })
        }
    }
}
