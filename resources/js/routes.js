import AdminHome from './components/admin/AdminHome.vue';
// Category
import CategoryMain from './components/admin/categories/Main.vue';
import CategoryList from './components/admin/categories/List.vue';
import NewCategory from './components/admin/categories/New.vue';
import Category from './components/admin/categories/View.vue';
// Post
import PostMain from './components/admin/post/Main.vue';
import PostList from './components/admin/post/List.vue';
import NewPost from './components/admin/post/New.vue';
import Post from './components/admin/post/View.vue';

// Frontend
import PublicHome from './components/public/PublicHome.vue';
import publicMaster from './components/public/PublicMaster.vue';
// Blog
import Blog from './components/public/blog/BlogHome.vue';
import SingleBlog from './components/public/blog/SingleBlog.vue';

export const routes = [{
        path: '/home',
        component: AdminHome
    },
    {
        path: '/category',
        component: CategoryMain,
        children: [{
                path: 'new',
                component: NewCategory
            },
            {
                path: '/',
                component: CategoryList
            },
            {
                path: ':id',
                component: Category
            }
        ]
    },
    {
        path: '/post',
        component: PostMain,
        children: [{
                path: '/',
                component: PostList
            },
            {
                path: 'new',
                component: NewPost
            },
            {
                path: ':id',
                component: Post
            }

        ]
    },
    // FrontEnd
    {
        path: '/',
        component: PublicHome
    },
    {
        path: '/blog',
        component: publicMaster,
        children: [{
                path: '/',
                component: Blog
            },
            {
                path: ':id',
                component: SingleBlog
            },
        ]
    },
    {
        path: '/categorypost/:catid',
        component: Blog
    }



]
