require('./bootstrap');
import Vue from 'vue';
// vue router used for routing
import VueRouter from 'vue-router';
Vue.use(VueRouter);
// vuex used for state management
import Vuex from 'vuex';
Vue.use(Vuex);
// Backend Routes
// import all routes form another file
import {
    routes
} from './routes';
// import state form another file
import StoreData from './store/index';
// main component used for main admin home
import MainApp from './components/admin/AdminMaster.vue';
Vue.component('admin-master', MainApp);
// FrontEnd Routes
// import {
//     froutes
// } from './froutes';
import MainHome from './components/public/PublicMaster.vue';
Vue.component('home-master', MainHome);


// moment js for formating time
import moment from 'moment'
// Vue filter 
Vue.filter('timeformat', (arg) => {
    return moment(arg).format('MMMM Do YYYY');
})
Vue.filter('postformat', (arg) => {
    return moment(arg).format('DD MMM YYYY');
})
Vue.filter('truncate', function (text, length, suffix) {
    return text.substring(0, length) + suffix;
});
// vform use for form validation
import {
    Form,
    HasError,
    AlertError
} from 'vform'
window.Form = Form
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
// sweet alert used for flash message/modal/popup ect
import swal from 'sweetalert2'
window.swal = swal;
const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
window.toast = toast


// import {
//     initialize
// } from './helpers/general';


const store = new Vuex.Store(StoreData);
const router = new VueRouter({
    routes,
    mode: 'history'
});

// initialize(store, router);

const app = new Vue({
    el: '#app',
    router,
    store,
    components: {
        MainApp,
        MainHome
    }
});
