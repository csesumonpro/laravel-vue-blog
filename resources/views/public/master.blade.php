<!DOCTYPE html>
<html lang="en">

<head>
    <title>Zeta</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Zeta Template Project">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/public')}}/styles/bootstrap4/bootstrap.min.css">
    <link href="{{asset('assets/public')}}/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/public')}}/plugins/OwlCarousel2-2.2.1/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/public')}}/plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/public')}}/plugins/OwlCarousel2-2.2.1/animate.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/public')}}/styles/main_styles.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/public')}}/styles/responsive.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/public')}}/plugins/js-flickr-gallery-1.24/js-flickr-gallery.css">
    <link href="{{asset('assets/public')}}/plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/public')}}/styles/blog_styles.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/public')}}/styles/blog_responsive.css">
</head>

<body>
    <div class="super_container" id="app">

        <!-- Header -->

        <header class="header d-flex flex-row justify-content-end align-items-center">

            <!-- Logo -->
            <div class="logo_container mr-auto">
                <div class="logo">
                    <a href="#"><span>z</span>zeta<span>.</span></a>
                </div>
            </div>

            <!-- Main Navigation -->
            <nav class="main_nav justify-self-end">
                <ul class="nav_items">
                    <li>
                        <router-link to="/"><span>HOME</span></router-link>
                    </li>
                    <li><a href="services.html"><span>services</span></a></li>
                    <li><a href="elements.html"><span>elements</span></a></li>
                    <li>
                        <router-link to="/blog"><span>BLOG</span></router-link>
                    </li>
                    <li><a href="contact.html"><span>contact</span></a></li>
                </ul>
            </nav>

            <!-- Hamburger -->
            <div class="hamburger_container">
                <span class="hamburger_text">Menu</span>
                <span class="hamburger_icon"></span>
            </div>

        </header>


        <!-- Menu -->

        <div class="fs_menu_overlay"></div>
        <div class="fs_menu_container">
            <div class="fs_menu_shapes"><img src="images/menu_shapes.png" alt=""></div>
            <nav class="fs_menu_nav">
                <ul class="fs_menu_list">
                    <li><a href="#"><span><span>H</span>Home</span></a></li>
                    <li><a href="#"><span><span>S</span>Services</span></a></li>
                    <li><a href="#"><span><span>E</span>Elements</span></a></li>
                    <li><a href="#"><span><span>B</span>Blog</span></a></li>
                    <li><a href="#"><span><span>C</span>Contact</span></a></li>
                </ul>
            </nav>
            <div class="fs_social_container d-flex flex-row justify-content-end align-items-center">
                <ul class="fs_social">
                    <li><a href="#"><i class="fab fa-pinterest trans_300"></i></a></li>
                    <li><a href="#"><i class="fab fa-facebook-f trans_300"></i></a></li>
                    <li><a href="#"><i class="fab fa-twitter trans_300"></i></a></li>
                    <li><a href="#"><i class="fab fa-dribbble trans_300"></i></a></li>
                    <li><a href="#"><i class="fab fa-behance trans_300"></i></a></li>
                    <li><a href="#"><i class="fab fa-linkedin-in trans_300"></i></a></li>
                </ul>
            </div>
        </div>

        <!-- Features -->

        <home-master></home-master>

    </div>
    <!-- Footer -->
    <footer class="footer">
        <div class="container">
            <div class="row footer_content d-flex flex-sm-row flex-column align-items-center">
                <div class="col-sm-6 cr text-sm-left text-center">
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;
                        <script>
                            document.write(new Date().getFullYear());
                        </script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i>                        by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
                <div class="col-sm-6 text-sm-right text-center">
                    <div class="footer_social_container">
                        <ul class="footer_social">
                            <li><a href="#"><i class="fab fa-pinterest trans_300"></i></a></li>
                            <li><a href="#"><i class="fab fa-facebook-f trans_300"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter trans_300"></i></a></li>
                            <li><a href="#"><i class="fab fa-dribbble trans_300"></i></a></li>
                            <li><a href="#"><i class="fab fa-behance trans_300"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in trans_300"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>



    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('assets/public')}}/js/jquery-3.2.1.min.js"></script>
    <script src="{{asset('assets/public')}}/styles/bootstrap4/popper.js"></script>
    <script src="{{asset('assets/public')}}/styles/bootstrap4/bootstrap.min.js"></script>
    <script src="{{asset('assets/public')}}/plugins/greensock/TweenMax.min.js"></script>
    <script src="{{asset('assets/public')}}/plugins/greensock/TimelineMax.min.js"></script>
    <script src="{{asset('assets/public')}}/plugins/scrollmagic/ScrollMagic.min.js"></script>
    <script src="{{asset('assets/public')}}/plugins/greensock/animation.gsap.min.js"></script>
    <script src="{{asset('assets/public')}}/plugins/greensock/ScrollToPlugin.min.js"></script>
    <script src="{{asset('assets/public')}}/plugins/progressbar/progressbar.min.js"></script>
    <script src="{{asset('assets/public')}}/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
    <script src="{{asset('assets/public')}}/plugins/easing/easing.js"></script>
    <script src="{{asset('assets/public')}}/js/custom.js"></script>

    <script src="{{asset('assets/public')}}/plugins/js-flickr-gallery-1.24/js-flickr-gallery.js"></script>
    <script src="{{asset('assets/public')}}/plugins/colorbox/jquery.colorbox-min.js"></script>
    <script src="{{asset('assets/public')}}/js/blog_custom.js"></script>
</body>
</body>

</html>