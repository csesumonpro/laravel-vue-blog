<footer class="footer">
    <div class="container">
        <div class="row footer_content d-flex flex-sm-row flex-column align-items-center">
            <div class="col-sm-6 cr text-sm-left text-center">
                <p>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;
                    <script>
                        document.write(new Date().getFullYear());
                    </script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i>                    by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </p>
            </div>
            <div class="col-sm-6 text-sm-right text-center">
                <div class="footer_social_container">
                    <ul class="footer_social">
                        <li><a href="#"><i class="fab fa-pinterest trans_300"></i></a></li>
                        <li><a href="#"><i class="fab fa-facebook-f trans_300"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter trans_300"></i></a></li>
                        <li><a href="#"><i class="fab fa-dribbble trans_300"></i></a></li>
                        <li><a href="#"><i class="fab fa-behance trans_300"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin-in trans_300"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

</div>

<script src="{{asset('assets/public')}}/js/jquery-3.2.1.min.js"></script>
<script src="{{asset('assets/public')}}/styles/bootstrap4/popper.js"></script>
<script src="{{asset('assets/public')}}/styles/bootstrap4/bootstrap.min.js"></script>
<script src="{{asset('assets/public')}}/plugins/greensock/TweenMax.min.js"></script>
<script src="{{asset('assets/public')}}/plugins/greensock/TimelineMax.min.js"></script>
<script src="{{asset('assets/public')}}/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="{{asset('assets/public')}}/plugins/greensock/animation.gsap.min.js"></script>
<script src="{{asset('assets/public')}}/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="{{asset('assets/public')}}/plugins/progressbar/progressbar.min.js"></script>
<script src="{{asset('assets/public')}}/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="{{asset('assets/public')}}/plugins/easing/easing.js"></script>
<script src="{{asset('assets/public')}}/js/custom.js"></script>
</body>

</html>