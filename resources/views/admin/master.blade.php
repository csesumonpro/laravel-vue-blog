<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin Home</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/app.css')}}" type="text/css">
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper" id="app">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
            <!-- Left navbar links -->
            <!-- SEARCH FORM -->
            <form class="form-inline ml-3">
                <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                    <div class="input-group-append">
                        <button class="btn btn-navbar" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                    </div>
                </div>
            </form>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="{{url('/home')}}" class="brand-link">
            <img src="{{asset('assets/admin/img/user.png')}}"
                 alt="AdminLTE Logo"
                 class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">{{Auth::user()->name}}</span>
        </a>

            <!-- Sidebar -->
            <div class="sidebar">

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                        {{--
                        <li class="nav-item has-treeview">--}} {{--
                            <a href="#" class="nav-link">--}}
                            {{--<i class="nav-icon fa fa-dashboard"></i>--}}
                            {{--<p>--}}
                                {{--Dropdown--}}
                                {{--<i class="right fa fa-angle-left"></i>--}}
                            {{--</p>--}}
                        {{--</a>--}} {{--
                            <ul class="nav nav-treeview">--}} {{--
                                <li class="nav-item">--}} {{--
                                    <a href="../../index.html" class="nav-link">--}}
                                    {{--<i class="fa fa-circle-o nav-icon"></i>--}}
                                    {{--<p>Dashboard v1</p>--}}
                                {{--</a>--}} {{--
                                </li>--}} {{--

                            </ul>--}} {{--
                        </li>--}}

                        <li class="nav-item">
                            <router-link to="/category" class="nav-link">
                                <i class="nav-icon fa fa-cat teal "></i>
                                <p>Category</p>
                            </router-link>

                        </li>
                        <li class="nav-item">
                            <router-link to="/post" class="nav-link">
                                <i class="nav-icon fab fa-blogger-b cyan"></i>
                                <p>Post</p>
                            </router-link>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                                                       document.getElementById('logout-form').submit();">
                              <i class="nav-icon fas fa-sign-out-alt red"></i>
                               {{__('Logout') }} 
                            </a>
                        </li>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <!-- Main content -->
            <section class="content">


                <admin-master></admin-master>



            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>Develop By</b> <a href="http://sumon-it.com">Sumon Sarker</a>
            </div>
            <strong>Copyright &copy; 2018 <a href="http://adminlte.io">Laravel Vue Blog</a>.</strong> All rights reserved.
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->


    <script src="{{asset('js/app.js')}}"></script>

</body>

</html>